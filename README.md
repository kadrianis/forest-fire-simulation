# Forest Fire Simulation

This project presents a simple, console-based forest fire simulation. The forest is represented as a 2D grid of trees, and at each step in the simulation, a tree can either be intact, on fire, or turned to ashes. The simulation ends when there are no more trees on fire.

## Approach

The simulation is divided into several key components: `Tree`, `Grid`, `State`, `ConfigLoader` and `Simulation`. 

1. **`Tree`**: Each tree is an object with a state (`State`) that can be intact, on fire, or ashes. The state of a tree can change according to certain rules (e.g., a tree can catch fire if it's next to a burning tree).

2. **`Grid`**: The grid represents the forest. It's a 2D array of trees. The grid is responsible for updating the state of all trees at each step of the simulation.

3. **`State`**: This is an enumeration that represents the possible states of a tree: intact, on fire, and ashes.

4. **`ConfigLoader`**: This class is responsible for loading the initial configuration of the simulation from a properties file. The properties file can specify the size of the forest and the initial position of fires.

5. **`Simulation`**: This is the main class that runs the simulation. It initializes the grid using the configuration loaded by `ConfigLoader`, displays the initial state of the forest, and then updates and displays the state at each step until no more trees are on fire.

## Simulation Steps

1. **Configuration Loading**: At the start, the simulation loads the configuration from a properties file. This includes the grid dimensions, the probability that a tree will catch fire, and the initial positions of the fires. If the configuration file has errors (e.g., non-integer values where integers are expected, or fire positions that exceed the grid dimensions), the program will display an error message and terminate.

2. **Grid Initialization**: The simulation initializes the grid (forest) based on the loaded configuration. 

3. **Display Initial State**: The simulation displays the initial state of the forest, showing which trees are intact, which are on fire, and which are already ashes. 

4. **Simulation Loop**: The simulation enters a loop where it updates the state of the forest at each step. At each step, the simulation displays the new state of the forest, along with the number of trees that are on fire, the number of burned trees (ashes), and the number of intact trees. The loop continues until there are no more trees on fire.

5. **Termination**: Once there are no more trees on fire, the simulation displays the total number of steps taken and ends.

The simulation provides a clear and simple visualization of how a forest fire can spread and eventually extinguish naturally. It's an excellent basis for further exploration or modification, such as adding wind direction or different types of vegetation.
